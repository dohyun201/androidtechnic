package com.example.androiddevpractice

import org.junit.Test
import java.lang.StringBuilder
import java.util.*

class CaseTest{
    @Test
    fun testcase(){
        var str = "abcdbbefgaBCDEFG123456"
        var hashtable : Hashtable<String, Int> = Hashtable()
        var strbuilder : StringBuilder = StringBuilder(str)
        var uniqueFlag = false

        for(index in 0..str.length-1){
            if(hashtable.get(strbuilder.get(index).toString()) !=null){
                uniqueFlag = false
                break
            }else{
                hashtable.put(strbuilder.get(index).toString(),index)
                uniqueFlag = true
              }
        }
        println("유일한지? = ${uniqueFlag}")
    }

    @Test
    fun testcase2(){
        var str = "aabccccccccaa"
        var count = 1
        var maxcount = -1

        for(i in 0 until str.length-1){
          println("str.get(i) = ${str.get(i)}")
          println("str.get(i+1) = ${str.get(i+1)}")

          if(str.get(i) == str.get(i+1)){
              count++
          }else{
              count=1
          }

           println("count = ${count}")
        }
    }

    @Test
    fun testcase1_6(){
        // 이미지를 표현하는 NxN 행렬 -> 각픽셀 4바이트 표현됨 , 이미지 90도 회전시키는 메소드
        /*
        * sol )
        *   1. 행렬 90도 회전이랑 같음
        *   2. 0,0
        *      a b c d
        *      e f g h
        *      i j k l
        *      m n o p
        *
        *   3. 0,0 -> 0,3  변경 행 <- 이전 열 / 변경 열 <- 마지막 행
        *      0,1 -> 1,3
        *      0,2 -> 2,3
        *      0,3 -> 3,3
        *
        *   4. 1,0 -> 0,2  다음 ->
        *      1,1 -> 1,2  변경 행 <- 이전 열
        *      1,2 -> 2,2  변경 열 <- 전체 행 -1
        *      1,3 -> 3,2
        *
        * */

        // 4x4 행열


        var imageArray = arrayOf(arrayOf(1,2,3,4),
                                 arrayOf(5,6,7,8),
                                 arrayOf(9,10,11,12),
                                 arrayOf(13,14,15,16))

        var newimageArray = Array(4,{ intArrayOf(0,0,0,0) })
//        println("newimageArray = ${newimageArray.contentDeepToString()}")

        println("pre = ${imageArray.contentDeepToString()}")

//        println("size = ${imageArray.size}")

        for(index in 0 until imageArray.size-1){
            for(jindex in 0 until imageArray.size){
                //행
                var newHang = jindex
                var newYeul = (imageArray.size-1)-index
                var temp = ""
                newimageArray[newHang][newYeul] = imageArray[index][jindex]
            }
        }

        println("post = ${newimageArray.contentDeepToString()}")

    }

    // 주어진 문자열내 모든 공백을 "%20" 으로 바꿔라 , 배열을 사용해서 해라
    // input : "Mr John Smith              " , 13 자리
    // output : Mr%20John%20Smith , 문자열 끝에서는 안붙음음
    @Test
    fun testcase3() {
        //case 1
        //var str = "Mr John Smith          "
        //case 2
        //var str = "          Mr John Smith          "
        //case 3
        var str = "          Mr J        o       hn Smith          "
        var trimLength = str.trim().length
        
        // 문자열 배열 사용
        var strArray: ArrayList<String> = ArrayList()
        var strbuilder = StringBuffer(str.trim())

        // 문자하나씩 배열에 넣기
        for (index in 0..str.trim().length - 1) {
                println("!!!! ${strbuilder[index]}")
                if (!strbuilder[index].isLetter()) {
                    strArray.add("%20")
                } else {
                    strArray.add(strbuilder[index].toString())
                }
        }

       var striterator = strArray.iterator()
       var newstrbuilder = StringBuffer()

        while (striterator.hasNext()){
            newstrbuilder.append(striterator.next())
       }

        println("newstrbuilder = ${newstrbuilder}")
    }

    @Test
    fun testcase4() {
        var str = "aabccccccccaaa"
    }

    @Test
    fun testcase1_7(){
        // MxN 행열 중 0의 값 이 있는경우 행 행, 열 모두 0으로 치환시키기
        var preArray = arrayOf(arrayOf(1,2,0),
                               arrayOf(2,3,4))
//        println("post = ${preArray.contentDeepToString()}")

        var postArray = preArray.clone()
//        println("post = ${postArray.contentDeepToString()}")


        for(index in 0..preArray.size-1){
            for(jindex in 0..preArray.size){

                if(preArray[index][jindex] == 0){
                    println("index = ${index}")
                    println("jindex = ${jindex}")

                    convertArray(index,jindex,preArray.size,postArray)
                    return
                }
            }
        }

        println("postArray = ${postArray.contentDeepToString()}")
    }

    fun convertArray(
        index: Int,
        jindex: Int,
        presize: Int,
        postArray: Array<Array<Int>>
    ) {
        for(p in 0 ..presize-1){
            println("p = ${p}")
//
            postArray[index][p] = 0 // 행고정
            postArray[p][jindex] = 0 // 열고정

            println("postArray11 = ${postArray.contentDeepToString()}")
        }
    }

    @Test
    fun case1_8(){
        var s1 = "erbottlewat"
        var s2 = "waterbottle"
        isSubstring(s1,s2)
    }

    private fun isSubstring(str: String, s2: String) {
        for(index in 0..str.length-1){
            var sp01 = str.substring(0,index)
            var sp02 = str.substring(index)
            var stringbuilder = StringBuilder()

            stringbuilder.append(sp02)
            stringbuilder.append(sp01)


            if(stringbuilder.toString().equals(s2)){
                println("회전시 같다!!!")
            }
        }
    }

}