package com.example.androiddevpractice.Algorithm

import org.junit.Test
import java.util.*

class TestCase {

    companion object{
       lateinit var inputValue:Any
    }
    @Test
    /* 123 -> 해싱함수 -> 테이블
                        키 , 벨류 에 저장
                        나중에 찾을때 키만 알면됨
                        하나의 값 버킷
        리소스를 포기하고 -> 속도를 증진
    */

    fun main(){
        /*
        *  A = [1,2,3,4]
        *  => 해싱함수
        *
        *  B = 해싱테이블 <KEY , VALUE>
        *
        *  눈에보이는 배열과의 차이는 굳이 인덱스를 0부터 할 필요가 없다는것
        *
        * */

        var testArray = intArrayOf(1,2,3,4,5)
        var hashTable : Hashtable<Int,Any> = Hashtable()
        var linkeddata : LinkedList<Int> = LinkedList()

        for(index in 0..testArray.size-1){
            var key = splitFunction(testArray[index])

//            // 이미 키를 썼다는건, 값이 있는다는것으로 확인!!!
//            if(hashTable[key] !=null){
//                inputValue = linkeddata.set(index, testArray[index])
//            }else{
//                inputValue = testArray[index]
//            }

            hashTable[key] = testArray[index]
        }

        println("hashTable = ${hashTable}")
        println("hashTable = ${hashTable.keys}")

    }

    private fun splitFunction(data : Int): Int {
        return (data/2)
    }
}