package com.example.androiddevpractice

import androidx.databinding.ObservableField

class UserObservable{
    val firstName = ObservableField<String>()
}