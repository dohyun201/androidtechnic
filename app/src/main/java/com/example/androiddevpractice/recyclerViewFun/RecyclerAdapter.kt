package com.example.androiddevpractice.recyclerViewFun

import android.content.Context
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.androiddevpractice.R
import kotlinx.android.synthetic.main.recycler_acitivty_main.view.*
import kotlinx.android.synthetic.main.recycler_item.view.*
import java.lang.Exception


class RecyclerAdapter(
    var context :Context,
    var items: ArrayList<Profile>
) : RecyclerView.Adapter<RecyclerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        println("?????????")
        var inflaterView = LayoutInflater.from(parent.context).inflate(R.layout.recycler_item,parent,false)
        return RecyclerViewHolder(inflaterView)
    }

    override fun getItemCount(): Int {
        println("iimageArray.size = ${items.size}")
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        println("imageArray[position].profileImg = ${items[position].profileImg}")
        println("imageArray[position].profileImg = ${items[position].age}")
        println("imageArray[position].profileImg = ${items[position].name}")

        Glide.with(context).
            load(items[position].profileImg)
            .centerCrop()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .listener(object : RequestListener<String,GlideDrawable>{
                override fun onException(
                    e: Exception?,
                    model: String?,
                    target: Target<GlideDrawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    Log.e("Image_exception",e.toString())
                    return false
                }

                override fun onResourceReady(
                    resource: GlideDrawable?,
                    model: String?,
                    target: Target<GlideDrawable>?,
                    isFromMemoryCache: Boolean,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }
            }).into(holder.itemView.list_imageview)
        holder.itemView.list_age.text = items[position].age.toString()
        holder.itemView.list_name.text = items[position].name.toString()
    }

}