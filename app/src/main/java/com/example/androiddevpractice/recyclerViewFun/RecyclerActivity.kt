package com.example.androiddevpractice.recyclerViewFun

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.androiddevpractice.R
import kotlinx.android.synthetic.main.recycler_acitivty_main.view.*

class RecyclerActivity : AppCompatActivity() {

    companion object{
        lateinit var binding : com.example.androiddevpractice.databinding.RecyclerAcitivtyMainBinding
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.recycler_acitivty_main)
        binding.recycleactivity = this

        var imageArray : ArrayList<Profile> = ArrayList()
        var defaultProfile = Profile("http://t1.daumcdn.net/friends/prod/editor/0efc4ce2-a09d-41ae-aace-e025193c07e5.jpg","라이언", 1)
        var defaultProfile1 = Profile("https://image.aladin.co.kr/product/18628/37/cover500/8952798260_1.jpg","라이언2", 1)
        var defaultProfile2 = Profile("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT0rexz0tYfHcyqP-oz3M6ZK4zLPzwGTaQH7GulU5SFr4gQNGDD","라이언3", 1)
        var defaultProfile3 = Profile("https://newsimg.sedaily.com/2019/01/23/1VE5F3W5WP_18.png","라이언4", 1)

        imageArray.add(defaultProfile)
        imageArray.add(defaultProfile1)
        imageArray.add(defaultProfile2)
        imageArray.add(defaultProfile3)

        var adapter = RecyclerAdapter(this.baseContext,imageArray)
        binding.root.recycler_listview.adapter = adapter
    }
}