package com.example.androiddevpractice.mvpPattern

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.androiddevpractice.R
import com.example.androiddevpractice.googlemvpPatterns.GoogleMvpActivity
import kotlinx.android.synthetic.main.google_mvp_activity_main.view.*
import kotlinx.android.synthetic.main.mvpppp_activity_main.view.*

class MvpPatternActivity : AppCompatActivity(), MvpPatternPresenter.View{

    /*
    * Impl 추가되는 경우
    * event -> view 에서 이벤트 받음
    * view -> presenter로 이벤트 내용 넘김
    * interface는 view만 사용하고 그것을 presenter에서 받고 처리
    * inteface에서 구현체를 presenterImpl 로 받아서 처리
    * view 에서 presenter.view 인터페이스 구현함
    * presenterImpl 처리후 view로 인터페이스 정의 함수 호출
    * view에서 화면 갱신
    *
    *
    * -> Google 정의랑 차이점 presenter를 더 세분화해서 presenterImpl 로 구현하여 사용
    * */
    companion object{
        lateinit var bindingMvpPattern : com.example.androiddevpractice.databinding.MvppppActivityMainBinding
        lateinit var mvpPresenters : MvpPatternPresenterImpl
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingMvpPattern = DataBindingUtil.setContentView(this, R.layout.mvpppp_activity_main)
        bindingMvpPattern.number = NumberMvp(0,0,0)
        bindingMvpPattern.mvpPatternActivity = this
        mvpPresenters = MvpPatternPresenterImpl(this)
    }

    fun sumFuns(num : NumberMvp){
        var firstNum = bindingMvpPattern.root.first.text.toString().toInt()
        var secondNum =bindingMvpPattern.root.second.text.toString().toInt()
        Log.e("sumFuction ===>","sumFuction 1122 ")
        mvpPresenters.loadItem(firstNum,secondNum)
    }

    override fun UpdateView(num : NumberMvp) {
        bindingMvpPattern.number = num
    }
}