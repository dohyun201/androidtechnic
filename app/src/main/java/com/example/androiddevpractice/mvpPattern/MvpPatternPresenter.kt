package com.example.androiddevpractice.mvpPattern

interface MvpPatternPresenter {
    fun loadItem(one :Int , two : Int)

    interface View {
        fun UpdateView(num : NumberMvp)
    }
}