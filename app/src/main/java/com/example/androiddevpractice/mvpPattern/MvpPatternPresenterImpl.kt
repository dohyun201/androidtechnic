package com.example.androiddevpractice.mvpPattern

class MvpPatternPresenterImpl (val view : MvpPatternPresenter.View): MvpPatternPresenter{

    override fun loadItem(one :Int , two : Int) {
        var sum = one + two
        var num = NumberMvp(one,two,sum)
        view.UpdateView(num)
    }
}