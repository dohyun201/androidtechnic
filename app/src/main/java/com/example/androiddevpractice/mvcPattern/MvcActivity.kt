package com.example.androiddevpractice.mvcPattern

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.androiddevpractice.R
import kotlinx.android.synthetic.main.mvc_activity_main.view.*


/*
*  Model : 데이터
*  View : 뷰 (엑티비티 ,프레그먼트)
*  Controller : 컨트롤러 (이벤트 처리기)
*
*  보통 웹에서는
*  1 > Controller 에서 이벤트 받음
*  2 > Model 거쳐서 데이터 갱신 여부 체크
*  3 > View 에서 데이터 업데이트 이지만
*
*  안드로이드에서는
*  1> M(VC) 같이 쓴다
*  2> Model : 데이터로만
*  3> View를 컨트롤러와 같이 씀
*
*  장점 > 패턴을 몰라도 확인가능
*       > 개발기간 짧아짐
*  단점 > 코드양 길어짐
*       > 스파게티 소스가 될수있다
*
* */

class MvcActivity : AppCompatActivity(){

    companion object{
        lateinit var binding : com.example.androiddevpractice.databinding.MvcActivityMainBinding
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.mvc_activity_main)
        binding.mvcActivity = this
    }

    fun sumFuns(number : Numbers){
        var mvcFirst = binding.root.firsts.text.toString().toInt()
        var mvcSecond = binding.root.seconds.text.toString().toInt()
        var sum = 0

        sum = mvcFirst + mvcSecond
        binding.number = Numbers(mvcFirst,mvcSecond,sum)
    }
}