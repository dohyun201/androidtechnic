package com.example.androiddevpractice

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.androiddevpractice.firebasePractice.FirebaseMessaging
import com.example.androiddevpractice.googlemvpPatterns.GoogleMvpActivity
import com.example.androiddevpractice.listviewpattern.ListViewActivity
import com.example.androiddevpractice.mvcPattern.MvcActivity
import com.example.androiddevpractice.mvpPattern.MvpPatternActivity
import com.example.androiddevpractice.recyclerViewFun.RecyclerActivity
import com.google.android.gms.common.GoogleApiAvailability

class StartActivity : AppCompatActivity(){

    companion object{
        lateinit var binding : com.example.androiddevpractice.databinding.StartActivityMainBinding
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.start_activity_main)
        binding.startactivty = this

        checkAndInitFirebase("Create")
    }

    override fun onResume() {
        super.onResume()
        checkAndInitFirebase("Resume")
    }

    private fun checkAndInitFirebase(type :String) {
        // 서비스 버전 낮으면 받기
        GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this)

        var firebase =  FirebaseMessaging().getFireabaseInstance()
        Log.e(type+ "token ==> ", firebase.toString())
    }

    fun googlemvp(view : View){
       intent = Intent(this,GoogleMvpActivity::class.java)
       startActivity(intent)
    }

    fun mvp(view : View){
        intent = Intent(this,MvpPatternActivity::class.java)
        startActivity(intent)
    }

    fun mvc(view : View){
        intent = Intent(this,MvcActivity::class.java)
        startActivity(intent)
    }

    fun recycler(view : View){
        intent = Intent(this,RecyclerActivity::class.java)
        startActivity(intent)
    }

    fun listview(view : View){
        intent = Intent(this,ListViewActivity::class.java)
        startActivity(intent)
    }
}