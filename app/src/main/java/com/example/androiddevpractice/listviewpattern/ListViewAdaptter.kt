package com.example.androiddevpractice.listviewpattern

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.androiddevpractice.R

class ListViewAdaptter(var mContext : Context, var data : ArrayList<User>) : BaseAdapter() {

    override fun getView(position: Int, converView: View?, parent: ViewGroup?): View {
        //inflater를 계속 호출 당한다. =>스크롤 올렸다 내릴때 마다 메모리가 계속 올라가는구만..

        var view = LayoutInflater.from(mContext).inflate(R.layout.listview_item,null)
        var profileImageview : ImageView = view.findViewById(R.id.profile_image_view)
        var nameTextview : TextView = view.findViewById(R.id.listview_id)
        var ageTextview: TextView = view.findViewById(R.id.listview_age)

        Glide.with(mContext).load(data[position].profileImg).into(profileImageview)
        nameTextview.text = data[position].name
        ageTextview.text = data[position].age.toString()

        return view
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return data.size
    }

}