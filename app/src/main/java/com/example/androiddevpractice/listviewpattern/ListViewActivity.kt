package com.example.androiddevpractice.listviewpattern

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.androiddevpractice.R
import com.example.androiddevpractice.databinding.ListviewActivityBinding
import kotlinx.android.synthetic.main.listview_activity.view.*

class ListViewActivity :AppCompatActivity(){

    companion object{
        lateinit var bindingss : ListviewActivityBinding
        var dataArray : ArrayList<User> = ArrayList()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingss = DataBindingUtil.setContentView(this, R.layout.listview_activity)

        dataArray.add(User("http://t1.daumcdn.net/friends/prod/editor/0efc4ce2-a09d-41ae-aace-e025193c07e5.jpg","라이언", 1))
        dataArray.add(User("https://image.aladin.co.kr/product/18628/37/cover500/8952798260_1.jpg","라이언2", 2))
        dataArray.add(User("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT0rexz0tYfHcyqP-oz3M6ZK4zLPzwGTaQH7GulU5SFr4gQNGDD","라이언3", 3))
        dataArray.add(User("https://newsimg.sedaily.com/2019/01/23/1VE5F3W5WP_18.png","라이언4", 4))
        dataArray.add(User("http://t1.daumcdn.net/friends/prod/editor/0efc4ce2-a09d-41ae-aace-e025193c07e5.jpg","라이언", 5))
        dataArray.add(User("https://image.aladin.co.kr/product/18628/37/cover500/8952798260_1.jpg","라이언2", 6))
        dataArray.add(User("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT0rexz0tYfHcyqP-oz3M6ZK4zLPzwGTaQH7GulU5SFr4gQNGDD","라이언3", 1))
        dataArray.add(User("https://newsimg.sedaily.com/2019/01/23/1VE5F3W5WP_18.png","라이언4", 1))
        dataArray.add(User("http://t1.daumcdn.net/friends/prod/editor/0efc4ce2-a09d-41ae-aace-e025193c07e5.jpg","라이언", 1))
        dataArray.add(User("https://image.aladin.co.kr/product/18628/37/cover500/8952798260_1.jpg","라이언2", 1))
        dataArray.add(User("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT0rexz0tYfHcyqP-oz3M6ZK4zLPzwGTaQH7GulU5SFr4gQNGDD","라이언3", 1))
        dataArray.add(User("https://newsimg.sedaily.com/2019/01/23/1VE5F3W5WP_18.png","라이언4", 1))
        dataArray.add(User("http://t1.daumcdn.net/friends/prod/editor/0efc4ce2-a09d-41ae-aace-e025193c07e5.jpg","라이언", 1))
        dataArray.add(User("https://image.aladin.co.kr/product/18628/37/cover500/8952798260_1.jpg","라이언2", 1))
        dataArray.add(User("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT0rexz0tYfHcyqP-oz3M6ZK4zLPzwGTaQH7GulU5SFr4gQNGDD","라이언3", 1))
        dataArray.add(User("https://newsimg.sedaily.com/2019/01/23/1VE5F3W5WP_18.png","라이언4", 1))
        dataArray.add(User("http://t1.daumcdn.net/friends/prod/editor/0efc4ce2-a09d-41ae-aace-e025193c07e5.jpg","라이언", 1))
        dataArray.add(User("https://image.aladin.co.kr/product/18628/37/cover500/8952798260_1.jpg","라이언2", 1))
        dataArray.add(User("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT0rexz0tYfHcyqP-oz3M6ZK4zLPzwGTaQH7GulU5SFr4gQNGDD","라이언3", 1))
        dataArray.add(User("https://newsimg.sedaily.com/2019/01/23/1VE5F3W5WP_18.png","라이언4", 1))
        dataArray.add(User("http://t1.daumcdn.net/friends/prod/editor/0efc4ce2-a09d-41ae-aace-e025193c07e5.jpg","라이언", 1))
        dataArray.add(User("https://image.aladin.co.kr/product/18628/37/cover500/8952798260_1.jpg","라이언2", 1))
        dataArray.add(User("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT0rexz0tYfHcyqP-oz3M6ZK4zLPzwGTaQH7GulU5SFr4gQNGDD","라이언3", 1))
        dataArray.add(User("https://newsimg.sedaily.com/2019/01/23/1VE5F3W5WP_18.png","라이언4", 1))
        dataArray.add(User("http://t1.daumcdn.net/friends/prod/editor/0efc4ce2-a09d-41ae-aace-e025193c07e5.jpg","라이언", 1))
        dataArray.add(User("https://image.aladin.co.kr/product/18628/37/cover500/8952798260_1.jpg","라이언2", 1))
        dataArray.add(User("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT0rexz0tYfHcyqP-oz3M6ZK4zLPzwGTaQH7GulU5SFr4gQNGDD","라이언3", 1))
        dataArray.add(User("https://newsimg.sedaily.com/2019/01/23/1VE5F3W5WP_18.png","라이언4", 1))
        dataArray.add(User("http://t1.daumcdn.net/friends/prod/editor/0efc4ce2-a09d-41ae-aace-e025193c07e5.jpg","라이언", 1))
        dataArray.add(User("https://image.aladin.co.kr/product/18628/37/cover500/8952798260_1.jpg","라이언2", 1))
        dataArray.add(User("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT0rexz0tYfHcyqP-oz3M6ZK4zLPzwGTaQH7GulU5SFr4gQNGDD","라이언3", 1))
        dataArray.add(User("https://newsimg.sedaily.com/2019/01/23/1VE5F3W5WP_18.png","라이언4", 1))
        dataArray.add(User("http://t1.daumcdn.net/friends/prod/editor/0efc4ce2-a09d-41ae-aace-e025193c07e5.jpg","라이언", 1))
        dataArray.add(User("https://image.aladin.co.kr/product/18628/37/cover500/8952798260_1.jpg","라이언2", 1))
        dataArray.add(User("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT0rexz0tYfHcyqP-oz3M6ZK4zLPzwGTaQH7GulU5SFr4gQNGDD","라이언3", 1))
        dataArray.add(User("https://newsimg.sedaily.com/2019/01/23/1VE5F3W5WP_18.png","라이언4", 1))
        dataArray.add(User("http://t1.daumcdn.net/friends/prod/editor/0efc4ce2-a09d-41ae-aace-e025193c07e5.jpg","라이언", 1))
        dataArray.add(User("https://image.aladin.co.kr/product/18628/37/cover500/8952798260_1.jpg","라이언2", 1))
        dataArray.add(User("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT0rexz0tYfHcyqP-oz3M6ZK4zLPzwGTaQH7GulU5SFr4gQNGDD","라이언3", 1))
        dataArray.add(User("https://newsimg.sedaily.com/2019/01/23/1VE5F3W5WP_18.png","라이언4", 1))
        dataArray.add(User("http://t1.daumcdn.net/friends/prod/editor/0efc4ce2-a09d-41ae-aace-e025193c07e5.jpg","라이언", 1))
        dataArray.add(User("https://image.aladin.co.kr/product/18628/37/cover500/8952798260_1.jpg","라이언2", 1))
        dataArray.add(User("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT0rexz0tYfHcyqP-oz3M6ZK4zLPzwGTaQH7GulU5SFr4gQNGDD","라이언3", 1))
        dataArray.add(User("https://newsimg.sedaily.com/2019/01/23/1VE5F3W5WP_18.png","라이언4", 1))
        dataArray.add(User("http://t1.daumcdn.net/friends/prod/editor/0efc4ce2-a09d-41ae-aace-e025193c07e5.jpg","라이언", 1))
        dataArray.add(User("https://image.aladin.co.kr/product/18628/37/cover500/8952798260_1.jpg","라이언2", 1))
        dataArray.add(User("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT0rexz0tYfHcyqP-oz3M6ZK4zLPzwGTaQH7GulU5SFr4gQNGDD","라이언3", 1))
        dataArray.add(User("https://newsimg.sedaily.com/2019/01/23/1VE5F3W5WP_18.png","라이언4", 1))

        var listadpater : ListViewAdaptter = ListViewAdaptter(this, dataArray)
        bindingss.root.listview_activity_view.adapter = listadpater
    }
}