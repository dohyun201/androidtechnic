package com.example.androiddevpractice

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : com.example.androiddevpractice.databinding.ActivityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        // 바인딩 세팅 필수!!
        binding.user = User("PDH",12)
        binding.activity =  this

    }

    fun onButtonClick(view :View){
        // 단순 매소드 참조
        Log.e("토스트!!!", "토스트!!!")
    }

    fun onSaveClick(user : User){
        // 매개변수를 전달하는 리스너 방식
        Log.e("토스트123", user.toString())
    }

    //activity.버튼명 ==> activity::버튼명

    // MVP 패턴
    // model : 데이터
    // view  : 액티비티나 ,프래그먼트
    // presenter  : 컨트롤러 같은 역할, 뷰내 이벤트를 위임

}
