package com.example.androiddevpractice.firebasePractice

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.androiddevpractice.R
import com.example.androiddevpractice.mvpPattern.MvpPatternActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FirebaseMessaging() : FirebaseMessagingService() {

    companion object {
        lateinit var notificationBuilder : NotificationCompat.Builder
    }

    var Tag = "FirebaseMessaging"
    //현재 토큰 검색
    var firebaseInstanceId = FirebaseInstanceId.getInstance().instanceId
        .addOnCompleteListener(OnCompleteListener { task ->
            if(!task.isSuccessful){
                Log.e(Tag,task.exception!!.message)
                return@OnCompleteListener
            }
            // 새로 토큰 받기
            val token = task.result?.token
            Log.e(Tag,token)
        })


    override fun onNewToken(token: String) {
       //토큰이 업데이트될때마다 호출되는 메소드
        Log.e("token update",token)
    }

    fun getFireabaseInstance(): Task<InstanceIdResult> {
        if(firebaseInstanceId ==null){
            FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if(!task.isSuccessful){
                        Log.e(Tag,task.exception!!.message)
                        return@OnCompleteListener
                    }
                    // 새로 토큰 받기
                    val token = task.result?.token
                    Log.e(Tag,token)
                })
        }
        return firebaseInstanceId
    }

    override fun onMessageReceived(message: RemoteMessage) {
        try{
            //메세지 받을때
            Log.e("onMessageReceived data", message.data.toString())
            showNotification(message)
        }catch (e : Exception){
            e.printStackTrace()
        }

        super.onMessageReceived(message)
    }

    private fun showNotification(message: RemoteMessage) {
        if(message !=null){
            try{
                // title , message
                var notificationManager: NotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

                var channelId = "under Oreo Channel id"

                var intent = Intent(this,MvpPatternActivity.javaClass)
                var pendingintent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT)
                var defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)


                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    var notificationChannel =
                        NotificationChannel("channel_id", "channel_name", NotificationManager.IMPORTANCE_HIGH)
                    notificationChannel.description = "channel Descriptoion"
                    notificationChannel.enableLights(true)
                    notificationChannel.lightColor = Color.RED
                    notificationChannel.enableVibration(true)
                    notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

                    notificationManager.createNotificationChannel(notificationChannel)

                    channelId = notificationChannel.id
                }

                notificationBuilder = NotificationCompat.Builder(this, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(message.data.get("title"))
                    .setContentText(message.data.get("messages"))
                    .setTicker("한줄 내용")
                    .setAutoCancel(true)
                    .setSound(defaultSound)
                    .setContentIntent(pendingintent)

                notificationManager.notify(0,notificationBuilder.build())

            }catch (e: java.lang.Exception){
                e.printStackTrace()
            }
        }
    }

}