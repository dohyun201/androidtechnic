package com.example.androiddevpractice.pagingLibrary.pagingSample.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.androiddevpractice.pagingLibrary.pagingSample.model.Repo
import com.google.gson.JsonArray

@Dao
interface RepoDao {

    @Query("SELECT * FROM repos WHERE (name LIKE :requestQuery)")
    fun reposByName(requestQuery: String): DataSource.Factory<Int, Repo>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(posts: List<Repo>)

}
