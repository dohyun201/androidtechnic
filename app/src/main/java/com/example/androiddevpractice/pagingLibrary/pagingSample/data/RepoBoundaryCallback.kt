package com.example.androiddevpractice.pagingLibrary.pagingSample.data

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.example.androiddevpractice.pagingLibrary.pagingSample.api.RestFulService
import com.example.androiddevpractice.pagingLibrary.pagingSample.api.getCafeList
import com.example.androiddevpractice.pagingLibrary.pagingSample.db.RestfulLocalCache
import com.example.androiddevpractice.pagingLibrary.pagingSample.model.Repo

class RepoBoundaryCallback(
    private val requestQuery: String,
    private val service: RestFulService,
    private val cache: RestfulLocalCache
) : PagedList.BoundaryCallback<Repo>() {

    companion object{
        val NETWORK_PAGE_SIZE = 50
    }

    private val _networkErrors = MutableLiveData<String>()
    val networkErrors: MutableLiveData<String> get() = _networkErrors
    var lastRequestPage = 1

    override fun onZeroItemsLoaded() {
        // 레트로핏통한 api 호출
        //requestAndSaveData(requestQuery)
    }

    override fun onItemAtEndLoaded(itemAtEnd: Repo) {
        requestAndSaveData(requestQuery)
    }

    override fun onItemAtFrontLoaded(itemAtFront: Repo) {
        super.onItemAtFrontLoaded(itemAtFront)
    }

    private fun requestAndSaveData(query: String) {
        Log.e("requestAndSaveData===>", "requestAndSaveData")
        // 레트로핏통한 api 호출
        getCafeList(service,
//                    query,
                    lastRequestPage,
                    NETWORK_PAGE_SIZE,{ repos ->
            cache.insert(repos){
                Log.e("repos ===> ", repos.toString())
                lastRequestPage++
            }
        },{ error ->
            networkErrors.postValue(error)
        })
    }



}
