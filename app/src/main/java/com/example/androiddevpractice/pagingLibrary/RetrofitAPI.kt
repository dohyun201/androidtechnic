package com.example.androiddevpractice.pagingLibrary

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface RetrofitAPI{

    //http://openapi.seoul.go.kr:8088/544274434c646f6839396351456270/json/coffeeShopInfo/1/5/
    @GET("/544274434c646f6839396351456270/json/coffeeShopInfo/1/{page}/")
    fun getCafeList(@Path("page") pageIndex : Int) : Call<JsonObject>

}