package com.example.androiddevpractice.pagingLibrary

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import org.json.JSONObject

data class CafeList(var cafeData:JsonObject)
