package com.example.androiddevpractice.pagingLibrary.pagingSample.db

import androidx.paging.DataSource
import com.example.androiddevpractice.pagingLibrary.pagingSample.db.RepoDao
import com.example.androiddevpractice.pagingLibrary.pagingSample.model.Repo
import java.util.concurrent.Executor

class RestfulLocalCache(
    private val reposDao: RepoDao,
    private val ioExecutor : Executor
) {
    fun reposByName(requestQuery: String): DataSource.Factory<Int, Repo> {
        return reposDao.reposByName(requestQuery)
    }

    fun insert(repos: List<Repo>, inserFinished: () -> Int) {
        ioExecutor.execute {
            reposDao.insert(repos)
            inserFinished
        }
    }

}
