package com.example.androiddevpractice.pagingLibrary.pagingSample.model

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.example.androiddevpractice.pagingLibrary.pagingSample.model.Repo

data class RepoSearchResult(
    val data: LiveData<PagedList<Repo>>?,
    val networkErrors: LiveData<String>?
)
