package com.example.androiddevpractice.pagingLibrary.pagingSample

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.example.androiddevpractice.pagingLibrary.pagingSample.api.RestFulService
import com.example.androiddevpractice.pagingLibrary.pagingSample.data.RestfulRepository
import com.example.androiddevpractice.pagingLibrary.pagingSample.db.RepoDatabase
import com.example.androiddevpractice.pagingLibrary.pagingSample.db.RestfulLocalCache
//import com.example.androiddevpractice.pagingLibrary.pagingSample.ui.ViewModelFactory
import java.util.concurrent.Executors

object injection {

    fun provideVieModelFactory(context: Context): ViewModelProvider.Factory? {
//        return ViewModelFactory(
//            provideRestFulRepository(context)
//        )
        return null
    }

    private fun provideRestFulRepository(context: Context): RestfulRepository {
        // 레트로핏 클라이언트
        return RestfulRepository(
            RestFulService.create(),
            provideCache(context)
        )
    }

    private fun provideCache(context: Context): RestfulLocalCache {
        // 룸데이터 베이스
        val database = RepoDatabase.getInstance(context)
        return RestfulLocalCache(database.reposDao(), Executors.newSingleThreadExecutor())
    }

}
