package com.example.androiddevpractice.pagingLibrary.pagingSample.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.example.androiddevpractice.pagingLibrary.pagingSample.data.RestfulRepository
import com.example.androiddevpractice.pagingLibrary.pagingSample.model.Repo
import com.example.androiddevpractice.pagingLibrary.pagingSample.model.RepoSearchResult

class SearchRepositoriesViewModel(private val repository : RestfulRepository) : ViewModel() {

    //transforation 은 기존데이터 받아서 새로운 데이터로 변형하는 역할
    private val queryLiveData = MutableLiveData<String>()
    private val repoResult : LiveData<RepoSearchResult> = Transformations.map(queryLiveData){ repository.search(it)}
    val repos : LiveData<PagedList<Repo>>? = Transformations.switchMap(repoResult){ it -> it.data}
    val networkErrors: LiveData<String> = Transformations.switchMap(repoResult){it -> it.networkErrors}

    fun searchRepo(query: String) {
        // 라이브데이터로 요청자 보냄
        queryLiveData.postValue(query)
    }

    fun lastQueryValue(): String? = queryLiveData.value


}
