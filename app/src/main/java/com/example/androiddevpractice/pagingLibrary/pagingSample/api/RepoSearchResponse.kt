package com.example.androiddevpractice.pagingLibrary.pagingSample.api

import com.example.androiddevpractice.pagingLibrary.pagingSample.model.Repo
import com.google.gson.annotations.SerializedName

data class RepoSearchResponse (
    @SerializedName("row") val items : List<Repo> = emptyList()
)
