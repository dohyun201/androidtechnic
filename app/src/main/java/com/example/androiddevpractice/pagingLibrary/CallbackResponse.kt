package com.example.androiddevpractice.pagingLibrary

import com.google.gson.JsonArray
import com.google.gson.JsonObject

interface CallbackResponse{
    fun onSuccesses(message: JsonArray)
    fun onfailed(t: Throwable)
}