package com.example.androiddevpractice.pagingLibrary.pagingSample.ui

import android.util.Log
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.androiddevpractice.pagingLibrary.pagingSample.model.Repo

class ResposAdapter : PagedListAdapter<Repo,RecyclerView.ViewHolder>(REPO_COMPARATOR) {
    companion object{
        //역할은 머지
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<Repo>(){
            // 특이하게 쓰네
            override fun areItemsTheSame(oldItem: Repo, newItem: Repo): Boolean =
                oldItem.name == newItem.name

            override fun areContentsTheSame(oldItem: Repo, newItem: Repo): Boolean =
                oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return RepoViewholder.create(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val repoitem = getItem(position)
        Log.e("repoitem===>", repoitem.toString());
        Log.e("onBindViewHolder===>", "onBindViewHolder");
        if(repoitem !=null){
            (holder as RepoViewholder).bind(repoitem)
        }
    }

}
