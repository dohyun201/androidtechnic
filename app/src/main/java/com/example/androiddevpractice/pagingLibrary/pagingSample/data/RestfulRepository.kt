package com.example.androiddevpractice.pagingLibrary.pagingSample.data

import android.util.Log
import androidx.paging.LivePagedListBuilder
import com.example.androiddevpractice.pagingLibrary.pagingSample.api.RestFulService
import com.example.androiddevpractice.pagingLibrary.pagingSample.db.RestfulLocalCache
import com.example.androiddevpractice.pagingLibrary.pagingSample.model.RepoSearchResult

class RestfulRepository(
    private val service : RestFulService,
    private val cache : RestfulLocalCache
){

    companion object{
       var DATABASE_PAGE_SIZE = 20
    }
    fun search(requestQuery: String): RepoSearchResult? {
        Log.e("Restful repository", requestQuery)

        // 캐시로부터 데이터 가져오기
        val dataSourceFactory = cache.reposByName(requestQuery)
        val boundaryCallback =
            RepoBoundaryCallback(
                requestQuery,
                service,
                cache
            )
        val networkErrors = boundaryCallback.networkErrors

        val data = LivePagedListBuilder(dataSourceFactory,
            DATABASE_PAGE_SIZE
        )
            .setBoundaryCallback(boundaryCallback)
            .build()

        return RepoSearchResult(data, networkErrors)
    }


}
