package com.example.androiddevpractice.pagingLibrary.pagingSample.ui

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.androiddevpractice.R
import com.example.androiddevpractice.databinding.SearchItemBinding
import com.example.androiddevpractice.pagingLibrary.pagingSample.model.Repo

lateinit var searchItemBinding : SearchItemBinding

class RepoViewholder(itemView : View) : RecyclerView.ViewHolder(itemView) {
    fun bind(repoitem: Repo) {
        Log.e("repoitem===>", repoitem.toString());
        searchItemBinding.searchlistTxt.text = repoitem.name
    }

    companion object{
        fun create(parent: ViewGroup): RepoViewholder{
            val view = LayoutInflater.from(parent.context).inflate(R.layout.search_item,parent,false)
            return RepoViewholder(view)
        }
    }
}
