package com.example.androiddevpractice.pagingLibrary

import android.util.Log
import com.google.gson.JsonObject
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitImpl{


    companion object{
       lateinit var retrofitInstance : Retrofit
       var baseUrl = "http://openapi.seoul.go.kr:8088"
       var authorizationKey = "544274434c646f6839396351456270"
       var context = "/json/jobCafeOpenInfo/"

    }


    fun initRetrofit(){
        var internalCeptor = HttpLoggingInterceptor()
        internalCeptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        var client = OkHttpClient.Builder().addInterceptor(internalCeptor).build()

        retrofitInstance =  Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .client(client)
            .build()
    }

    fun getRtrofit(): RetrofitAPI{
        retrofitInstance.let {
            return it.create(RetrofitAPI::class.java)
        }
    }

    fun reqCafeList(pageIndex: Int, callbackRes : CallbackResponse){

        getRtrofit().getCafeList(pageIndex).enqueue(
            object : Callback<JsonObject> {
                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    Log.e("onFailure==>","onFailure")
                    callbackRes.onfailed(t)
                }

                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    Log.e("onResponse==>",response.body().toString())
                    if(response.isSuccessful){
                        var cafeList = response.body()!!.getAsJsonObject("coffeeShopInfo").getAsJsonArray("row")
                        callbackRes.onSuccesses(cafeList)
                    }
                }
            })
    }
}