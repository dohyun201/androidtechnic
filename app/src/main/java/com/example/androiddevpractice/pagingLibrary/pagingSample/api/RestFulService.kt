package com.example.androiddevpractice.pagingLibrary.pagingSample.api

import android.util.Log
import com.example.androiddevpractice.pagingLibrary.pagingSample.model.Repo
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


fun getCafeList(
                service: RestFulService,
//                query: String,
                lastRequestPage: Int,
                NETWORK_PAGE_SIZE: Int,
                onSuccess: (repos:List<Repo>) -> Unit,
                onError: (error:String) -> Unit
) {
        service.getCafeList(lastRequestPage).enqueue(
            object :retrofit2.Callback<JsonObject>{
                override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                    Log.e("onResponse==>", response.body().toString())
                    var cafeList = response.body()!!.getAsJsonObject("coffeeShopInfo").getAsJsonArray("row")
                    //JSONARRAY -> [] 로
                    var newList :ArrayList<Repo> = ArrayList()

                    for(obj in cafeList){
                       Log.e("데이터1", obj.asJsonObject["NM"].toString())
                       Log.e("데이터2", obj.asJsonObject["ID"].toString())
                       newList.add(Repo(obj.asJsonObject["ID"].asLong,obj.asJsonObject["NM"].asString))
                    }

                    onSuccess(newList)
                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    Log.e("onFailure==>", t.message)
                    onError(t.message ?: "unknown error")
                }
            }
        )
}

interface RestFulService {

    @GET("/544274434c646f6839396351456270/json/coffeeShopInfo/1/{page}/")
    fun getCafeList(@Path("page") pageIndex : Int) : Call<JsonObject>


    companion object{
        private const val BASE_URL = "http://openapi.seoul.go.kr:8088"

        fun create() : RestFulService {
            // 레트로핏 클라이언트 생성
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BODY

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(RestFulService::class.java)
        }
    }
}
