package com.example.androiddevpractice.pagingLibrary

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.size
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androiddevpractice.R
import com.example.androiddevpractice.databinding.ActivityPagingMainBinding
import com.google.gson.JsonArray
import kotlinx.android.synthetic.main.activity_paging_main.view.*

class PagingActivity : AppCompatActivity(){
    // 무한 스크롤 테스트용도
    companion object{
        lateinit var pagingBinding : ActivityPagingMainBinding
        var indexCount = 16
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pagingBinding = DataBindingUtil.setContentView(this, R.layout.activity_paging_main)
        pagingBinding.activityPaging = this
        initRetrofit()
        infiniteScroll(pagingBinding)
    }

    private fun initRetrofit() {
        RetrofitImpl().initRetrofit()
        requestCafeList(indexCount)
    }

    private fun requestCafeList(endindex :Int){
        Log.e("endindex==>", endindex.toString())
        RetrofitImpl().reqCafeList(endindex, object : CallbackResponse{
            override fun onSuccesses(message: JsonArray) {
                Log.e("onSuccesses==>", message.toString())
                var recyclerview = pagingBinding.root.paging_recyceler_view
                recyclerview.adapter = PagingAdapter(message)

                Log.e("canScrollVertically==>", recyclerview.canScrollVertically(1).toString())
                Log.e("indexCount==>", indexCount.toString())

            }

            override fun onfailed(t: Throwable) {
                Log.e("onfailed==>",t.message)
            }
        })
    }

    private fun infiniteScroll(pagingBinding: ActivityPagingMainBinding) {
        var recyclerview = pagingBinding.root.paging_recyceler_view
        recyclerview.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                // 현재뷰의 마지막 카운트 포지션
                // 로딩된 아이템 포지션
                var lastVisibleItemPosition = ((recyclerView.layoutManager) as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
                var itemTotalCount = recyclerView.adapter!!.itemCount-1
                Log.e("lastItemPosition==>", lastVisibleItemPosition.toString())
                Log.e("itemTotalCount=>", itemTotalCount.toString())

                // 최하단 위치
                if(!recyclerview.canScrollVertically(1)){
                    //recyclerview.scrollToPosition(itemTotalCount)
                    if(lastVisibleItemPosition == itemTotalCount){
                        // 마지막 데이터 도달
                        Log.e("마지막 데이터 도달==>","마지막 데이터 도달")
                        indexCount += 5
                        requestCafeList(indexCount)
                    }
                    recyclerview.smoothScrollToPosition(indexCount)
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
    }


}