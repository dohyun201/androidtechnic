package com.example.androiddevpractice.pagingLibrary

import androidx.paging.ItemKeyedDataSource

/*
* DataSource 는 어디서 어떻게 데이터를 가져올지 하는 단계
* 방법은 3가지
* 각 방법마다 사용용도가 다름
* PageKyeDataSource : 다음 , 이전 토큰값이 존재할떄
* ItemKeyedDataSource : N-1 / N+1 번쨰 데이터 가져올떄
* PositionalDataSource  : 특정위치 데이터 가져올때
*
* 사용이유: 2억만개 이상을 로딩할때, 한번에 로딩하게되면 부담이 된다.
* 따라서 서버는 해당 인덱스를 받아서, 어디까지 로딩을 시키는데
* 안드로이드는 기존 방식은 스크롤에 대한 이벤트 (마지막 스크롤까지 왔을때 새로 로딩하는 방식) 을 썼다.
*
* -> 그래서 페이징라이브러리는 한번에 보여지는 덩어리만 로딩해놓고, 필요할때 로딩할수있게한다
*    굳이 스크롤 안해도
* */

class PagingiItemKeyDatasource : ItemKeyedDataSource<Long, DailyItem>() {
    override fun loadInitial(
        params: LoadInitialParams<Long>,
        callback: LoadInitialCallback<DailyItem>
    ) {
        // 최초 데이터 로딩시 callback의 onResult로 콜백줌
    }

    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<DailyItem>) {
       // 스크롤 내릴때 데이터 로딩
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<DailyItem>) {
        // 스크롤 올릴때 데이터 로딩
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getKey(item: DailyItem): Long {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
