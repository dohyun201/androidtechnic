package com.example.androiddevpractice.pagingLibrary.pagingSample.ui

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.androiddevpractice.R
import com.example.androiddevpractice.databinding.SearchActivityMainBinding
import com.example.androiddevpractice.pagingLibrary.pagingSample.injection
import com.example.androiddevpractice.pagingLibrary.pagingSample.model.Repo

class SearchRepositoryActivity : AppCompatActivity(){

    lateinit var searchActitityBinding: SearchActivityMainBinding
    private val customAdapter = ResposAdapter()
    lateinit var viewModel : SearchRepositoriesViewModel

    companion object{
        const val LAST_SEARCH_QUERY = "last_search_query"
        const val DEFAULT_QUERY = "Android"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        searchActitityBinding = DataBindingUtil.setContentView(this,R.layout.search_activity_main)

        // 뷰모델 가져옴 -> 팩토리패턴에서 인스턴스 생성하는데 -> 레포지토리 인자로 넣음
        viewModel = ViewModelProviders.of(this,injection.provideVieModelFactory(this))
            .get(SearchRepositoriesViewModel::class.java)

        // 리스트끼리 구분선 추가
        val decoration = DividerItemDecoration(this,DividerItemDecoration.VERTICAL)
        searchActitityBinding.searchRecyclerview.addItemDecoration(decoration)

        // 리사이클러뷰 어뎁터 초기화
        initAdapter()

        // 쿼리 던져서 검색하려고 씀
        val query = savedInstanceState?.getString(LAST_SEARCH_QUERY) ?: DEFAULT_QUERY
        viewModel.searchRepo(query)

        initSearch(query)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(LAST_SEARCH_QUERY,viewModel.lastQueryValue())
    }

    // 어뎁터 및 뷰모델 옵저버블 세팅
    private fun initAdapter() {
        searchActitityBinding.searchRecyclerview.adapter = customAdapter

        // 뷰모델내 데이터를 옵져버함
        viewModel.repos!!.observe(this,Observer<PagedList<Repo>>{
            Log.e("it?.size===>", it?.size.toString())
            showEmptyList(it?.size  == 0)
        })

        // 뷰모델내 통신에러를 옵져버함
        viewModel.networkErrors.observe(this,Observer<String>{
            Log.e("통신에러!!!","통신에러!!")
        })
    }

    private fun showEmptyList(show: Boolean) {
        // 데이터가 비어있는지 , 없는지 체크해서 뭘 보여줄지 결정
        if(show){
            searchActitityBinding.noShowText.visibility = View.VISIBLE
            searchActitityBinding.searchRecyclerview.visibility = View.GONE
        }else{
            searchActitityBinding.noShowText.visibility = View.GONE
            searchActitityBinding.searchRecyclerview.visibility = View.VISIBLE
        }
    }


    private fun initSearch(query: String?) {
        // 초기 검색 로직
        // 액션중, 바로가기 액션 일경우 검색
        searchActitityBinding.searchInputText.setOnEditorActionListener { textView, actionId, keyEvent ->
            if(actionId == EditorInfo.IME_ACTION_GO){
               updateRepoListFromInput()
                true
            }else{
                false
            }
        }

        // 자판중 엔타자판 선택시 검색되게 함
        searchActitityBinding.searchInputText.setOnKeyListener { view, keyCode, event ->
            if(event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER){
                updateRepoListFromInput()
                true
            }else{
                false
            }
        }
    }

    // 검색되게 하는 로직
    private fun updateRepoListFromInput() {
        // 검색어가 있으면 검색
        searchActitityBinding.searchInputText.text.trim().let {
            if(it.isNotEmpty()){
                searchActitityBinding.searchRecyclerview.scrollToPosition(0)
                viewModel.searchRepo(it.toString())
                customAdapter.submitList(null)
            }
        }
    }
}