package com.example.androiddevpractice.pagingLibrary

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.androiddevpractice.R
import com.google.gson.JsonArray
import kotlinx.android.synthetic.main.paging_recycler_item.view.*

class PagingAdapter(var data :JsonArray) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var wrapper =  LayoutInflater.from(parent.context).inflate(R.layout.paging_recycler_item,parent,false)
        return PagingViewHolder(wrapper)
    }

    override fun getItemCount(): Int {
        return data.size()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var cafeDataJson = CafeList(data.get(position).asJsonObject)
        holder.itemView.cafe_name.text = cafeDataJson.cafeData.get("NM").asString
    }

}
