package com.example.androiddevpractice.googlemvpPatterns

class MainPresenter : MainContract.Presenter{
    var views : MainContract.View? = null

    override fun setView(view: MainContract.View) {
        this.views = view
    }

    override fun loadItem(firstNum: Int,secondNum: Int){
        var sum = firstNum+secondNum
        var number =  NumberGoogle(firstNum,secondNum,sum)
        this.views!!.updateView(number)
    }
}