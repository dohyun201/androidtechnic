package com.example.androiddevpractice.googlemvpPatterns

interface MainContract {
    interface View{
        fun updateView(sumNumberGoogle: NumberGoogle)
    }

    interface Presenter{
        fun setView(view : View)
        fun loadItem(firstNum: Int,secondNum: Int)
    }
}
