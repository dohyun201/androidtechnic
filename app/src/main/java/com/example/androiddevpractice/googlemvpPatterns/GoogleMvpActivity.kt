package com.example.androiddevpractice.googlemvpPatterns

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.androiddevpractice.R
import com.example.androiddevpractice.mvpPattern.NumberMvp
import kotlinx.android.synthetic.main.google_mvp_activity_main.view.*

class GoogleMvpActivity : AppCompatActivity(), MainContract.View{


    /*
    * 구글이 정의하는 MVP 방식
    * 1> View에서 -> setview 호출
    * 2> 이벤트 발생 -> 데이터 ->presenter 로 넘김
    * 3> presenter에서 계산 및 비즈니스 로직 처리
    * 4> presenter에서 presenter.view 상속받은후 -> view에 있는 메소드 호출
    * 5> view 에서 view 갱신
    * */

    companion object{
        lateinit var  bindingMvpGoogle : com.example.androiddevpractice.databinding.GoogleMvpActivityMainBinding
        lateinit var  mainPresenter : MainPresenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingMvpGoogle = DataBindingUtil.setContentView(this,
            R.layout.google_mvp_activity_main
        )
        bindingMvpGoogle.googlenumber = NumberGoogle(0, 0, 0)
        bindingMvpGoogle.googlemvpActivity = this

        mainPresenter = MainPresenter()
        mainPresenter.setView(this)
    }

    fun sumFuction(number : NumberGoogle){
        var firstNum = bindingMvpGoogle.root.googlefirst.text.toString().toInt()
        var secondNum = bindingMvpGoogle.root.googlesecond.text.toString().toInt()
        Log.e("sumFuction ===>","sumFuction")
        mainPresenter.loadItem(firstNum,secondNum)
    }

    override fun updateView(sumNumberGoogle: NumberGoogle) {
        Log.e("number ===>", sumNumberGoogle.toString())
        bindingMvpGoogle.googlenumber = sumNumberGoogle
    }
}