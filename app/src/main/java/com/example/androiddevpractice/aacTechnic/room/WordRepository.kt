package com.example.androiddevpractice.aacTechnic.room

import androidx.lifecycle.LiveData

//네트워크에서 데이터 가져오기위한 repository 사용

// ***** 쿼리를 통한 데이터 엑세스는 dao만 통해서 해야함 *****
class WordRepository (private val wordDao : WordDao){

    // 데이터 가져온것 새로운 변수에 담음 , livedata로 변경을 알려줌
    val allwords : LiveData<List<Word>> = wordDao.getAlphabetizedWords()

    // suspend 는 코루틴인데, 이것은 잘모르겠다.

    // ********************
    // 서브루틴 : 하나의 시작점이 있어서 종료되는것, 함수내 소스가 진행 시작 -> 끝
    // 코루틴 : 시작이 있다가, 갑자기 진행된곳에서 멈추고, 다시 멈춘곳에서 재개할수 있는 것 , 백만개의 쓰레드도 돋릴수있다!!
    // 블로킹 : ex> 아이디받음 -> 토큰 요청 -> 토큰 받음 -> 로그인
    // 위의 프로세스라면, 아이디받는거 요청하고 놀고, 토큰 요청하고 놀고 등등의 시간낭비가 있다.
    // suspend를 붙이면 이함수를 코루틴으로 작동할거기 떄문에 순차적인 코드를 짜더라도 알아서 기능 처리 된이후에 자동으로 재개된다.
    // ********************

    suspend fun insert(word:Word){
        // view -> viewmodel -> 레포지토리 -> dao ->
        wordDao.insert(word)
    }
}