package com.example.androiddevpractice.aacTechnic.room

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.androiddevpractice.R
import kotlinx.android.synthetic.main.recycler_room_item.view.*

/*
* 평범한 어뎁터 형식
* setWord가 호출되서, list에 값 세팅된다.
* */
class WordListApdater internal constructor(
    context : Context
): RecyclerView.Adapter<WordListApdater.WordViewHolder>(){

    private val inflater : LayoutInflater = LayoutInflater.from(context)
    private var words = emptyList<Word>()

    inner class WordViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val wordItemView : TextView = itemView.findViewById(R.id.textView_room)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): WordViewHolder {
        val itemView = inflater.inflate(R.layout.recycler_room_item, parent, false)
        return WordViewHolder(itemView)
    }

    override fun getItemCount() : Int {
        return words.size
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        val current = words[position]
        holder.wordItemView.text = current.word
    }

    internal fun setWords(words:List<Word>){
        this.words = words
        notifyDataSetChanged()
    }

}