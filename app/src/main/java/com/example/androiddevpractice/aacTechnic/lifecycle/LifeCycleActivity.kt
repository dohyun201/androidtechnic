package com.example.androiddevpractice.aacTechnic.lifecycle

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.androiddevpractice.R
import com.example.androiddevpractice.databinding.LifeCycleActivityMainBinding
import kotlinx.android.synthetic.main.life_cycle_activity_main.view.*

class LifeCycleActivity : AppCompatActivity(){

    companion object{
        lateinit var binding:LifeCycleActivityMainBinding
    }

    // AAC뷰모델을 쓰면, 라이프사이클 및 화면 구성 상관없이 데이터가 계속 보존된다.
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@LifeCycleActivity, R.layout.life_cycle_activity_main)
        binding.root.chronomter.start()

        var chorometerViewModel = ViewModelProvider(this)[ChronometerViewmodel::class.java]

        if(chorometerViewModel.getStartTime() ==null){
            chorometerViewModel.setStartTime(0)
            binding.root.chronomter.base = 0
        }else{
            binding.root.chronomter.base = chorometerViewModel.getStartTime()
        }
    }
}