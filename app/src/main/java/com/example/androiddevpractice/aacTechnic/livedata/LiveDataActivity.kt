package com.example.androiddevpractice.aacTechnic.livedata

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.androiddevpractice.R
import com.example.androiddevpractice.databinding.LivedataActivitiyMainBinding
import com.google.android.gms.common.util.DataUtils
import kotlinx.android.synthetic.main.livedata_activitiy_main.view.*

class LiveDataActivity : AppCompatActivity(){

    companion object{
        lateinit var binding : LivedataActivitiyMainBinding
        lateinit var mMode :UserProfileViewModel
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.livedata_activitiy_main)
        binding.liveDataActivity = this

        mMode = ViewModelProvider(this).get(UserProfileViewModel::class.java)
//        var nameObserver = Observer<String> { str -> binding.root.livedata_text.text = str }
        var nameObserver = object : Observer<String>{
            override fun onChanged(str: String?) {
                binding.root.livedata_text.text = str
            }
        }
        mMode.getCurrentName().observe(this,nameObserver)
    }

    fun changeName(view : View){
        mMode.getCurrentName().value = "pd"
    }

}