package com.example.androiddevpractice.aacTechnic.lifecycle_livedata

import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.widget.Chronometer
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.*
import com.example.androiddevpractice.R
import com.example.androiddevpractice.databinding.ComposeActivityMainBinding
import kotlinx.android.synthetic.main.compose_activity_main.view.*

class ComposeActivity :AppCompatActivity(), LifecycleObserver {

    companion object{
        lateinit var binding:ComposeActivityMainBinding
        lateinit var model : TimeViewmodel
        lateinit var timeSwitch : Chronometer
        lateinit var lifecycleowner : LifecycleOwner
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // data binding
        binding = DataBindingUtil.setContentView(this, R.layout.compose_activity_main)
        binding.compostActivity = this
        var timeSwitch = binding.root.timeChronomter
        var recyclerList = binding.root.recycler_print_list

        // lifecycle
        model = ViewModelProvider(this).get(TimeViewmodel::class.java)

        if(model.getCurrentTime().value == null){
            model.setCurrentTime(TimeModel(SystemClock.elapsedRealtime()))
            timeSwitch.base = SystemClock.elapsedRealtime()
        }else{
            timeSwitch.base = model.getCurrentTime().value!!.mTime
        }

        // livedata 적용
        val timeobserver =
            Observer<TimeModel> { timemodel ->
                Log.e("onChanged","onChanged")
                Log.e("onChanged", timemodel!!.mTime.toString())
                var lifecycle = lifecycleowner.lifecycle.currentState
                Log.e("current lifecycle", lifecycle.toString())
            }

        //viewmodel 구독
        model.getCurrentTime().observe(this,timeobserver)
        timeSwitch.start()

        //lifecycle 구독
        lifecycleowner = this
        lifecycleowner.lifecycle.addObserver(this)
    }

    fun initBtn(view :View){
        timeSwitch = view.rootView.timeChronomter
        timeSwitch.base = SystemClock.elapsedRealtime()
    }

    fun timeStart(view :View){
        timeSwitch = view.rootView.timeChronomter
        timeSwitch.start()
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun init(){
        Log.e("ON_CREATE 1==>", "create!!")
        timeSwitch = binding.root.timeChronomter
        timeSwitch.base = 0
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun timePause(){
        timeSwitch = binding.root.timeChronomter
        timeSwitch.stop()
        Log.e("ON_PAUSE 1==>", "stop!!")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun timereStart(){
        timeSwitch = binding.root.timeChronomter
        timeSwitch.base = SystemClock.elapsedRealtime()
        timeSwitch.start()
        Log.e("ON_RESUME 1==>", "restart!!")
    }


    fun timeStop(view :View){
        timeSwitch = view.rootView.timeChronomter
        timeSwitch.stop()
        model.setCurrentTime(TimeModel(model.getCurrentTime().value!!.mTime))
        Log.e("data 1==>", model.getCurrentTime().value!!.mTime.toString())
        Log.e("data 2==>", timeSwitch.base.toString())
    }

    fun timePrint(view :View){
        Log.e("data==>", model.getCurrentTime().value!!.mTime.toString())

    }
}