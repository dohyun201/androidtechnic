package com.example.androiddevpractice.aacTechnic.room

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.example.androiddevpractice.R
import com.example.androiddevpractice.databinding.RoomMainActivityBinding
import kotlinx.android.synthetic.main.room_main_activity.view.*


/*
* 단순히 데이터만 입력받아서 넘겨주는 역할
* */
class RoomMainActivity :AppCompatActivity(){

    //ActivityRommMainBinding
    companion object{
        lateinit var bindings : RoomMainActivityBinding
        lateinit var editText : EditText
        lateinit var eventBtn : Button
        const val EXTRA_REPLY = "com.example.android.wordlistsql.REPLY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindings = DataBindingUtil.setContentView(this, R.layout.room_main_activity)
        bindings.roommainAcitivity = this
        editText = bindings.root.edit_word
        eventBtn = bindings.root.button_save
    }

    fun btnEvent(view : View){
        val replyintent = Intent()
        if(TextUtils.isEmpty(editText.text)){
            setResult(Activity.RESULT_CANCELED,replyintent)
        }else{
            val word = editText.text.toString()
            replyintent.putExtra(EXTRA_REPLY, word)
            setResult(Activity.RESULT_OK, replyintent)
        }

        finish()
    }
}