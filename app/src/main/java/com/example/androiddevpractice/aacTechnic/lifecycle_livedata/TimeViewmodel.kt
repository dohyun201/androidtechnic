package com.example.androiddevpractice.aacTechnic.lifecycle_livedata

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TimeViewmodel :ViewModel(){

    var mutableLiveData = MutableLiveData<TimeModel>()

    fun getCurrentTime(): MutableLiveData<TimeModel> {
        if(mutableLiveData ==null){
           mutableLiveData = MutableLiveData<TimeModel>()
        }

        return mutableLiveData
    }

    fun setCurrentTime(timeModel: TimeModel) {
        mutableLiveData.value = timeModel
    }
}