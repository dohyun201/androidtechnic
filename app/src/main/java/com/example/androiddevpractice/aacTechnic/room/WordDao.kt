package com.example.androiddevpractice.aacTechnic.room

import androidx.lifecycle.LiveData
import androidx.room.*

// DAO는 데이터액세스 객체 : 쿼리랑 직접 연결하는 클래스
@Dao
interface WordDao {

    // 어노테이션, @DELETE, @UPDATE , @INSERT 는 따로 쿼리문 안써도됨

    @Query("SELECT * from word_table ORDER BY word ASC")
    // livedata로 포장해서 실시간으로 데이터 갱신하게 한다
    fun getAlphabetizedWords() : LiveData<List<Word>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(word:Word)

    @Query("DELETE FROM word_table")
    suspend fun deleteAll()

}