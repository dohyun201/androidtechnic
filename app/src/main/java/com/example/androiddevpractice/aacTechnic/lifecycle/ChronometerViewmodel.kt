package com.example.androiddevpractice.aacTechnic.lifecycle

import androidx.lifecycle.ViewModel

class ChronometerViewmodel : ViewModel(){
    private  var mStartTime : Long = 0L

    fun setStartTime(time : Long){
        mStartTime = time
    }

    fun getStartTime() : Long{
        return mStartTime
    }

}


