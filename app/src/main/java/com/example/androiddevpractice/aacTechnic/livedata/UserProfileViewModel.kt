package com.example.androiddevpractice.aacTechnic.livedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class UserProfileViewModel :ViewModel(){

    private var mutableLiveData = MutableLiveData<String>()

    fun getCurrentName() : MutableLiveData<String>{
        if(mutableLiveData == null){
            mutableLiveData = MutableLiveData<String>()
        }

        return mutableLiveData
    }
}