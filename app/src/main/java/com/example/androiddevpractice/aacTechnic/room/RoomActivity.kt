package com.example.androiddevpractice.aacTechnic.room

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androiddevpractice.R
import com.example.androiddevpractice.databinding.ActivityRommMainBinding
import kotlinx.android.synthetic.main.activity_romm_main.view.*

/*
* 메인화면
* databinding + activity / recyclerview
* recyclerview -> adapater
* apdater -> 새로운 어뎁터
* lifecycele로 데이터 유지되게함
* viewModel로 observe함
*
* 데이터 바인딩 버튼 : 내용추가 액티비티 이동
* -> 다시 돌아올때
* */
class RoomActivity : AppCompatActivity(){

    companion object{
        lateinit var binding : ActivityRommMainBinding
        lateinit var wordviewModel : WordViewModel
        const val newWordAcitivtyRequestCode = 1
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_romm_main)
        binding.roomActivity = this

        var recyceler =  binding.root.recyclerview_room
        var adapters = WordListApdater(this)
        recyceler.adapter = adapters

        wordviewModel = ViewModelProvider(this).get(WordViewModel::class.java)

        // allWord라는 변수를 옵져버 세팅
        wordviewModel.allWords.observe(this, Observer {
            //람다
            // 인자 (타입 생략가능) -> { 본문 }
            // 람다의 인자가 한개일경우 it으로 대체 가능 ! it은 그러니까 words
            // insert 하면서 변경 감지되니까, 데이터 추가된다.
            words -> words?.let { adapters.setWords(it) }
        })
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == newWordAcitivtyRequestCode && resultCode == Activity.RESULT_OK){
            data?.getStringExtra(RoomMainActivity.EXTRA_REPLY)?.let {
            // let 은 null 값 체크 대신 쓰기때문에, null 이 아닐떄만 작동한다는 규칙!!
            // viewModel 에 세팅해서 , 생명주기 안거치고
                val word = Word(it)
                wordviewModel.insert(word)
            }
        }else{
            Toast.makeText(applicationContext
                ,R.string.empty_not_saved,
                Toast.LENGTH_LONG).show()
        }
    }

    fun floatingBtn(view : View){
        val intent = Intent(this@RoomActivity,RoomMainActivity::class.java)
        startActivityForResult(intent,newWordAcitivtyRequestCode)
    }
}