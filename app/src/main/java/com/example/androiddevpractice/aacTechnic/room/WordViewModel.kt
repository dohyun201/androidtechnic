package com.example.androiddevpractice.aacTechnic.room

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class WordViewModel (application: Application) : AndroidViewModel(application){

    private val repository : WordRepository
    // 라이브 데이터로 감싸서 실시간으로 데이터 옵져버블
    val allWords: LiveData<List<Word>>

    init{
        // 초기화 내용

        // getDatabase -> 싱글톤 (룸 라이브러리)
        val wordsDao = WordRoomDatabase.getDatabase(application,viewModelScope).wordDao()
        // 레파지토리
        repository = WordRepository(wordsDao)
        allWords = repository.allwords
    }

    //launch 는 코루틴을 하기위한 빌더
    //코루틴은 실행 정지 재개를 할수있는 서브루틴과 다른 작업
    fun insert(word:Word) = viewModelScope.launch{
        repository.insert(word)
    }
}