package com.example.androiddevpractice.aacTechnic.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "word_table") // 테이블명
data class Word(@PrimaryKey @ColumnInfo(name="word") var word :String)

//@PrimaryKey - 기본키
// @ColumnInfo(name="wod") = 테이블 열의 이름, 변수가 다를경우 사용

