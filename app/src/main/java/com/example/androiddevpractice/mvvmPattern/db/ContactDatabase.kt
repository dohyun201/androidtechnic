package com.example.androiddevpractice.mvvmPattern.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.androiddevpractice.mvvmPattern.data.Contact

/*
* database를 객체화
* entities = 테이블명 -> 테이블은 클래스화 -> 데이터 클래스명 contact
* */
@Database(entities = [Contact::class], version = 1)
abstract class ContactDatabase : RoomDatabase(){

    abstract fun contactDao() : ContactDao

    /*
    *  데이터베이스 객체를 싱글인스턴스롤 생성해서 쓰기 위함
    *  companion 객체로 생성
    *  Room 라이브러리와 연동해서 builder 생성
    * */
    companion object{
        private var INSTANCE : ContactDatabase? = null

        fun getInstance(context: Context) : ContactDatabase? {
            if(INSTANCE == null){
                /*
                * synchroized 여러 쓰레드가 접근하지 못하도록
                * */
                synchronized(ContactDatabase::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        ContactDatabase::class.java,
                        "contact")
                         // 데이터 베이스가 갱신될때, 기존 테이블 버리고 새로 사용
                        .fallbackToDestructiveMigration()
                        .build()
                }
            }
            return INSTANCE
        }
    }

}