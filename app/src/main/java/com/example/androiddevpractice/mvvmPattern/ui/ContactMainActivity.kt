package com.example.androiddevpractice.mvvmPattern.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.androiddevpractice.R
import com.example.androiddevpractice.databinding.ContactActivityMainBinding
import com.example.androiddevpractice.mvvmPattern.data.Contact
import com.example.androiddevpractice.mvvmPattern.model.ContactViewModel

class ContactMainActivity : AppCompatActivity(){


    /*
    * 전체적인 구조 :
    *  activity : viewModel세팅 -> viewModel 내부 data 감지 걸어둠
    *  data 변경 <-> Repository <-> Dao <-> 쿼리 <-> 데이터
    *
    * */

    lateinit var contactViewModel : ContactViewModel
    lateinit var contactBinding : ContactActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // 데이터 바인딩
        contactBinding = DataBindingUtil.setContentView(this, R.layout.contact_activity_main)

        /*
        *  뷰모델 세팅
        *  옵저버블로 데이터 가져올떄 데이터 감지
        *  viewModelProvider 를 통해서 viewMoedl 객체를 안드로이드 시스템에서 관리하게 함
        * */
        contactViewModel = ViewModelProvider(this).get(ContactViewModel::class.java)
        contactViewModel.getAll().observe(this, Observer<List<Contact>>{
          //  contact -> // ui update
        })

    }

    fun btnAddUser(view : View){

    }
}