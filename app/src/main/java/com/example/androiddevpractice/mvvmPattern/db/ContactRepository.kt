package com.example.androiddevpractice.mvvmPattern.db

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.androiddevpractice.mvvmPattern.data.Contact
import java.lang.Exception

class ContactRepository(application : Application) {

    private val contactDatabase = ContactDatabase.getInstance(application)!!

    // 싱글톤으로 만든 데이터베이스 클래스 안에, dao를 추상화 클래스로 만들어서 구현체와 연결
    private val contactDao : ContactDao = contactDatabase.contactDao()

    // liveData 로 매핑해서, viewModel에서 감지할수 있게하고, 그데이터는 dao를 통해 쿼리호출로 가져온 데이터로 매핑
    private val contacts : LiveData<List<Contact>> = contactDao.getAll()

    /*
    * 핵심 구조 1!!
    * Repository -> dao -> 쿼리 -> 데이터 가져옴
    * 쓰레드를 쓴이유는 메인쓰레드로 db접근시 앱크래시가 있기떄문에
    * */

    fun getAll() : LiveData<List<Contact>>{
        return contacts
    }

    fun insert(contact : Contact){
        try{
            val thread = Thread(Runnable {
                contactDao.insert(contact)
            })
            thread.start()
        } catch ( e: Exception){
            e.printStackTrace()
        }
    }

    fun delete(contact : Contact){
        try{
            val thread = Thread(Runnable {
                contactDao.delete(contact)
            })
        }catch (e : Exception){
            e.printStackTrace()
        }
    }
}