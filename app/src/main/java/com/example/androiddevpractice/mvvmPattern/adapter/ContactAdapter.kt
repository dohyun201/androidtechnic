package com.example.androiddevpractice.mvvmPattern.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.recyclerview.widget.RecyclerView
import com.example.androiddevpractice.R
import com.example.androiddevpractice.mvvmPattern.data.Contact

class ContactAdapter(val contactitemCLick : (Contact) -> Unit, val contactItemLongClickListener: (Contact) -> Unit): RecyclerView.Adapter<ContactVIewHolder>(){

    private var contacts : List<Contact> = listOf()
    lateinit var contactVIewHolder : ContactVIewHolder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactVIewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.contact_activity_main,parent,false)
        contactVIewHolder = ContactVIewHolder(view)
        return ContactVIewHolder(view)
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    override fun onBindViewHolder(holder: ContactVIewHolder, position: Int) {
        contactVIewHolder.bind(contacts[position])
    }

}