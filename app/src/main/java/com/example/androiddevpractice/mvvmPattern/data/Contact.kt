package com.example.androiddevpractice.mvvmPattern.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/*
* entity 데이터 클래스 생성
*
* */

// 컬럼명을 적는데, 클래스 명이랑 동일하기 떄문에 보통 생략
@Entity(tableName = "contact")
data class Contact(
    /*
    * db 테이블을 만드는데,
    * db 컬럼명을 생성한다.
    * */

    // autoIncrement 로 자동으로 db에 추가 될때마다 증가
    @PrimaryKey(autoGenerate = true)
    var id : Long?,

    // 컬럼명 설정
    @ColumnInfo(name = "name")
    var name : String,

    @ColumnInfo(name = "number")
    var number : String,

    @ColumnInfo(name = "initial")
    var initial : Char


    /*
    * db 테이블이며,
    * 현제 테이블 명은 contact 이고
    * 테이블내 컬럼명은 id, name , number, initial 이 들어감
    * */
){
    constructor() : this(null, "", "", '\u0000')
}