package com.example.androiddevpractice.mvvmPattern.model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.androiddevpractice.mvvmPattern.db.ContactRepository
import com.example.androiddevpractice.mvvmPattern.data.Contact

class ContactViewModel (application: Application) : AndroidViewModel(application){

    /*
    * context 를 가져오는데, application 으로 가져와야 메모리 릭이 없음
    * viewModel -> repository -> Dao -> 쿼리 -> 데이터
    *           <-           <-     <-      <-
    * 개발 순서는 뒤부터 만드는데, 실제적인 구조는 위와 같음
    * 단순하게 view -> viewmodel 을 통해 data 변경 확인 -> liveData이용
    *
    * */
    private val repository =
        ContactRepository(application)
    private val contacts = repository.getAll()

    fun getAll() : LiveData<List<Contact>>{
        return this.contacts
    }

    fun insert(contact: Contact){
        repository.insert(contact)
    }

    fun delete(contact : Contact){
        repository.delete(contact)
    }
}