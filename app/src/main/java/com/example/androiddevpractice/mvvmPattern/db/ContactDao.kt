package com.example.androiddevpractice.mvvmPattern.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.androiddevpractice.mvvmPattern.data.Contact

/*
* Dao 인터페이스로 생성 -> sql 을 함수형으로 표시 함
* sql 쿼리문과 연동하기 위한 dao 생성
* 함수형으로 쿼리를 호출하고, Select 쿼리문은 Query로 표시
* insert 삽입
* delete 삭제
* onConflict 는=> 중복데이터 처리에 대한 정책 처리
* */

@Dao
interface ContactDao {

    // 데이터를 가져오는데 LIveData로 감싸주는 이유는 viewModel 에서 데이터 변화를 계속 감지하기 위함
    @Query("SELECT * FROM contact ORDER BY name ASC")
    fun getAll(): LiveData<List<Contact>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(contact : Contact)

    @Delete
    fun delete(contact : Contact)
}