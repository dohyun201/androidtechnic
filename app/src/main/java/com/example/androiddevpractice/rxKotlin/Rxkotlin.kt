package com.example.androiddevpractice.rxKotlin

import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.rxkotlin.toObservable

class Rxkotlin {
    fun main(){
        basicItorator()
        println("========= rxKotlin!!!!==========")
        rxObserver()
    }

    fun basicItorator(){
        var arr : List<Any> = listOf("1","2","#",33,"abc")
        var iteratorArr = arr.iterator()

        while(iteratorArr.hasNext()){
            println(iteratorArr.next())
        }
    }


    // iterator 쓰는대신, observable 써서 -> 더 직관적으로 데이터 관찰
    // 변경사항을 푸시한다!!! 이게 핵심
    private fun rxObserver() {
        var arr : List<Any> = listOf("1","2","#",33,"abc")
        var observable  = arr.toObservable()
        observable.subscribeBy(
            onNext = {println(it)},
            onComplete = { print("Done!!")},
            onError = {it.printStackTrace()}
        )


    }

}